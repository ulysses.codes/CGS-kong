# Version Checker

A tool that checks if an asset is up to date, and opens a download link if it isn't.

## Google sheet
This tool works off of a [google sheet](https://docs.google.com/spreadsheets/d/1ZQk7UzCWJt9IxTlUqOa4e_SYqq9ElR_TFniL9vLVROs/edit?usp=sharing). The google sheet contains the version information and google drive links for all the assets.

### Columns
**Asset**: filename / name of the asset

**Version**: current version of the asset

**Link**: link generated from google drive

### Generating a link
1. Go to the google drive folder containing the asset
2. Right click the asset name
3. Click "Get Link"
4. Click "Copy Link"
5. Paste the link in the **Link** column of the google sheet

### Supported assets
This list will expand in the future
- `.abc`
- single frame `.bgeo`

## Parameters

### Refresh
Pressing this checks [the google doc](https://docs.google.com/spreadsheets/d/1ZQk7UzCWJt9IxTlUqOa4e_SYqq9ElR_TFniL9vLVROs/edit?usp=sharing) for the latest version, and if you don't have the latest version will open the google drive link that's in the folder.

### Status
Lists the current version, or the error message if there is one. If there's an error you don't understand, email [ulysses@ulysses.me](mailto:ulysses@ulysses.me)

### File
The filename - use this just like you'd use any other file loader, except **do not include the version number**
