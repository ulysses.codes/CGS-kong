Skip to [[#^b37b26]] for a list of currently implemented tools

# Ideas

[[Asset Version]]
- automatically update Google Sheets with asset version numbers
- Houdini integration with an indication on a node if there's an old version

[[Dailies]]
- render/flipbook daily progress pdg
- add `shot`, `task`, `artist_name`, `date` to rendered images
- run automatically using node-red

[[Interesting Info]]
- add point/parameter info to a render
- use drop down tools and accept pasting elements

# Implemented

^b37b26